export function cartChange(): void {
    cy.server();
    cy.route({
      method: 'POST',
      url: '**/carts/changeQuantityCommand',
    }).as('cartChange')     
}