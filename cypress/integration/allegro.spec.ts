import { cartChange } from '../support/mock';

describe('Interview - allegro test', () => {
  beforeEach(() => {
    cy.setCookie('gdpr_permission_given', '1');
    cy.viewport(1920, 1080);
    cy.server();
    cartChange();
  });
    it('Visit allegro and add first 2 items to a cart', () => {
      cy.visit('https://allegro.pl');
      cy.get('[data-role="search-input"]').type('kwiat');
      cy.get('[data-role="search-button"]').click();
      cy.get('article').eq(0).click();
      cy.get('#add-to-cart-button').click();
      cy.wait('@cartChange');
      cy.get('[data-box-name="precart-layer"] [data-role="modal"]').should('be.visible');
      cy.go('back');
      cy.get('article').eq(1).click();
      cy.get('#add-to-cart-button').click();
      cy.wait('@cartChange');
      cy.get('[data-box-name="precart-layer"] [data-role="modal"]').should('be.visible');
      cy.get('[data-role="modal"] [href="/koszyk"]').click();
      cy.get('offer-title').should('have.length', 2);
      });
    });